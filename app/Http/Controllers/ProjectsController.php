<?php

namespace App\Http\Controllers;

use App\Projectuser;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Project;
use App\Task;
use App\Credential;
use App\Helpers\Helpers;

class ProjectsController extends BaseController {

	// Returns the given project view
	public function show($id)
	{   	
		$project 		=	Project::find($id);

        // Must be refactored as a filter
		if ( $project->isOwner() == false && $project->isMember() == false ) {
			return Redirect::to('/hud');
		}

		return  View::make('ins/projects/show')->with('pTitle', $project->name);
	}

	// Get all user projects
	public function getAllUserProjects(){
		$projects = Project::where('user_id',Auth::id())->get();

		if($projects) {
			foreach ($projects as $project) {
				$completedWeight = Project::find($project->id)->tasks()->where('state','=','complete')->sum('weight');
				$totalWeight = Project::find($project->id)->tasks()->sum('weight');

				$project["completedWeight"] = $completedWeight;
				$project["totalWeight"] = $totalWeight;
			}
		}

		return $this->setStatusCode(200)->makeResponse('پروژه با موفقیت بازیابی شد.',$projects->toArray());
	}

    // Get all projects that the Auth user is a member of
	public function getAllMemberProjects(){
        $sharedProjects = Projectuser::where('user_id', Auth::id())->select('project_id')->get();
        $project_ids = [];

        foreach($sharedProjects as $project){
            $project_ids[] = $project->project_id;
        }

        $sharedProjects = Project::whereIn('id', $project_ids)->get();

        if($sharedProjects) {
            foreach ($sharedProjects as $project) {
                $completedWeight = Project::find($project->id)->tasks()->where('state','=','complete')->sum('weight');
                $totalWeight = Project::find($project->id)->tasks()->sum('weight');

                $project["completedWeight"] = $completedWeight;
                $project["totalWeight"] = $totalWeight;
            }
        }
        return $this->setStatusCode(200)->makeResponse('پروژه ها با موفقیت بازیابی شد.',$sharedProjects);
    }

	//	Return the given project
	public function getProject($id){
		if (!Project::find($id)) {
			return $this->setStatusCode(404)->makeResponse('پروژه مورد نظر یافت شند.');
		}

		$project = Project::find($id);
		$project->tasks = Task::where('project_id', $id)->get();
		$project->credentials = Credential::where('project_id', $id)->get();

		return $this->setStatusCode(200)->makeResponse('پروژه با موفقیت یافت شد.', $project);
	}

	// Insert the given project into the database
	public function storeProject(){
		if (!Input::all() || strlen(trim(Input::get('name'))) == 0) {
			return $this->setStatusCode(406)->makeResponse('اطلاعات کافی برای  ایجاد پروژه جدید وارد نشده است.');
		}

		Input::merge(array('user_id' => Auth::id()));
		Project::create(Input::all());
		$id = \DB::getPdo()->lastInsertId();

		return $this->setStatusCode(200)->makeResponse('پروژه با موفقیت ساخته شد.', Project::find($id));
	}

	// Update the given project
	public function updateProject($id){
		if ( Input::get('name') === "") {
			return $this->setStatusCode(406)->makeResponse('پروژه به یک نام نیاز دارد');
		}

		if (!Project::find($id)) {
			return $this->setStatusCode(404)->makeResponse('پروژه یافت نشد.');
		}

		$input = Input::all();
		unset($input['_method']);

		Project::find($id)->update($input);
		return $this->setStatusCode(200)->makeResponse('پروژه با موفقیت به روز رسانی شد.');
	}

    public function getOwner($id){
        $owner_id = Project::whereId($id)->pluck('user_id');
        $owner = User::whereId($owner_id)->get();

        return $this->setStatusCode(200)->makeResponse('ok.', $owner[0]);
    }

    public function getMembers($id){
        $members_id = Projectuser::where('project_id', $id)->lists('user_id');
        $members = [];

        foreach($members_id as $id){
            $member = User::whereId($id)->get();
            array_push($members, $member[0]);
        }

        return $this->setStatusCode(200)->makeResponse('ok.', $members);
    }
    // Invites a user to the given project.
	public function invite($project_id, $email){
        if(trim(strlen($email)) == 0){
            return $this->setStatusCode(406)->makeResponse('فیلد ایمیل الزامیست!');
        }

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->setStatusCode(406)->makeResponse('لطفا یک ایمیل معتبر وارد نمایید.');
        }

        $project_name	= Project::find($project_id)->pluck('name');
        $owner_id	    = Project::find($project_id)->pluck('user_id');
        $project_url 	= url() . '/projects/'.$project_id;
        $invited_user   = User::whereEmail($email)->get();

        if( count($invited_user) == 0 ){
            return $this->setStatusCode(406)->makeResponse('حساب کاربر مورد نظر موجود نیست.');
        }
        $invited_user = $invited_user[0];

        if( count(Projectuser::whereUserId($invited_user->id)->whereProjectId($project_id)->get()) != 0 ){
			return $this->setStatusCode(406)->makeResponse('کابر با این ایمیل قبلا دعوت شده است.');
		}

        if(Auth::id() != $owner_id){
            return $this->setStatusCode(406)->makeResponse('فقط ایجیاد کننده پروژه میتواند سایر کاربران را به پروژه دعوت نمایید.');
        }
		// Save the relationship between user and project.
		$pu				= 	new Projectuser();
		$pu->project_id	=	$project_id;
		$pu->user_id	=	$invited_user->id;
		$pu->save();

		Helpers::sendProjectInviteMail($email, $project_name, $project_url);
		return $this->setStatusCode(200)->makeResponse('یک عضو جدید به پروژه اضافه شده است.', $invited_user);
	}

    // Removes a member from a given project
	public function removeMember($project_id, $member_id){
		if( count(Projectuser::whereUserId($member_id)->whereProjectId($project_id)->get()) == 0 ){
			return $this->setStatusCode(406)->makeResponse('کاربر مورد نظر در پروژه وجود ندارد.');
		}

		$project = Project::find($project_id);
		$project->members()->detach($member_id);

		return $this->setStatusCode(200)->makeResponse('عضو مورد نظر از پروژه حذف شد.');
	}

}