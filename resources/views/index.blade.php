@extends('templates.outs.home')

@section('content')
    {{-- HEADER--}}
	<div class="hug hug-header" style="min-height: 100vh; padding-top: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-xs-4  col-xs-offset-4" style="text-align: center; line-height: 55px;">
                    <h5 style="text-align: center; line-height: 55px;">به نام خدا</h5>
                    <img src="{{asset('assets/img/logo.jpg')}}" alt="دانشگاه آل طه">
                    <h5>عنوان پروژه:</h5>
                    <h3 style="text-align: center; line-height: 55px;">طراحی سیستم مدیریت پروژه</h3>
                    <h4 style="text-align: center; line-height: 55px;">استاد: جناب آقاي دكتر حامد اروجلو</h4>

                    <h5 style="text-align: center; line-height: 55px;">طراحی و پیاده سازی: شهرزاد میرفیضی</h5>
                    <div style="margin: 0 auto; display: table">
                        <a href="{{ route('login') }}" class="btn btn-primary btn-line pull-right login">ورود</a>
                        <a href="{{ route('register') }}" class="btn btn-primary btn-line pull-right register">عضویت</a>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
	</div>
@stop