@extends('templates/ins/master')

@section('content')
    <div id="user">
        <div class="row">
            <div class="col-xs-12 page-title-section">
                <h1 class="pull-right">تنظیمات</h1>
                <a v-on:click="update()" class="btn btn-primary pull-left" title="دخیره">ذخیره تغییرات</a>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="row settings-container">
            <div class="col-xs-12">
                <section>
                <div class="col-xs-12 col-md-4 left-side">
                    <a href="{{ route('profile') }}"><img class="circle" src="{{ App\User::get_gravatar(Auth::user()->email) }}"></a>
                    <div class="info">
                        <p class="name">@{{ user.full_name }}</p>
                        <p class="color-primary">@{{ user.email }}</p>
                        <p class="color-primary">@{{ user.title }}</p>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="links">
                        <p v-if="user.link">
                            <a target="_blank" href="">
                                <span class="ion-ios-world-outline"></span> @{{ user.link }}
                            </a>
                        </p>
                        <p v-if="user.twitter">
                            <a target="_blank" href="https://twitter.com/@{{ user.twitter }}">
                                <span class="ion-social-twitter"></span> https://twitter.com/@{{ user.twitter }}
                            </a>
                        </p>
                        <p v-if="user.facebook">
                            <a target="_blank" href="https://www.facebook.com/people/@{{ user.facebook }}">
                                <span class="ion-social-facebook"></span> https://www.facebook.com/people/@{{ user.facebook}}
                            </a>
                        </p>
                        <p v-if="user.linkedin">
                            <a target="_blank" href="https://www.linkedin.com/in/@{{ user.linkedin }}">
                                <span class="ion-social-linkedin"></span> https://www.linkedin.com/in/@{{ user.linkedin }}
                            </a>
                        </p>
                    </div>
                    <hr>

                    <div class="bio">
                        <p>بیوگرافی<p>
                        <p>@{{ user.bio }}  </p>
                    </div>

                </div>
                <div class="col-xs-12 col-md-8 right-side">
                    <div class="mega-menu">
                        <p v-if="msg.error != null" class="status-msg error-msg">@{{ msg.error }}</p>
                        <p v-if="msg.success != null" class="status-msg success-msg">@{{ msg.success }}</p>
                        <div class="links">
                            <a class="" data-id="settings_info" href="">اطلاعات شخصی</a>
                            <a class="" data-id="settings_links" href="">لینک ها</a>
                            <a class="" data-id="settings" href="">تنظیمات حساب</a>
                        </div>
                        <div class="content">
                            <div class="item" id="settings_info">
                                <div class="form">
                                    <form>
                                        <div class="form-group">
                                            <label>نام و نام خانوادگی</label>
                                            <input v-model="user.full_name" type="text" class="form-control" placeholder="نام و نام خانوادگی">
                                        </div>
                                        <div class="form-group">
                                            <label>ایمیل</label>
                                            <input v-model="user.email" type="text" class="form-control" placeholder="ایمیل">
                                        </div>
                                        <div class="form-group">
                                            <label>عنوان</label>
                                            <input v-model="user.title" type="title" class="form-control" placeholder="عنوان">
                                        </div>
                                        <div class="form-group">
                                            <label>بیوگرافی</label>
                                            <textarea v-model="user.bio" class="form-control" rows="7" placeholder="یک مقداری درباره من ..."></textarea>
                                            <br>
                                            <span class="count pull-right">@{{ 250 - user.bio.length }}</span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="item" id="settings_links">
                                <div class="input-group">
                                    <span class="input-group-addon">لینک</span>
                                    <input v-model="user.link" type="text" class="form-control" placeholder="آدرس وبسایت شما">
                                </div><br>
                                <div class="input-group">
                                    <span class="input-group-addon">https://twitter.com/</span>
                                    <input v-model="user.twitter" type="text" class="form-control" placeholder="نام کاربری تویتر">
                                </div><br>
                                <div class="input-group">
                                    <span class="input-group-addon">https://facebook.com/</span>
                                    <input v-model="user.facebook" type="text" class="form-control" placeholder="نام کاربری فیسبوک">
                                </div><br>
                                <div class="input-group">
                                    <span class="input-group-addon">https://Linkedin.com/</span>
                                    <input v-model="user.linkedin" type="text" class="form-control" placeholder="نام کاربری لینکدین">
                                </div>
                            </div>
                            <div class="item" id="settings">
                                <label>رمز فعلی</label>
                                {!! Form::open(array('action' => array('UsersController@resetPassword', Auth::id() ))) !!}
                                <div class="form-group">
                                    {!! Form::password( 'current_pwd', array('class' => 'form-control', "placeholder" => "رمز فعلی" )) !!}
                                </div>
                                <label>رمز جدید</label>
                                <div class="form-group">
                                    {!! Form::password( 'new_pwd', array('class' => 'form-control', "placeholder" => "رمز جدید" )) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit( 'تغییر رمز', array('class' => 'btn btn-default pull-right')) !!}
                                    <div class="clearfix"></div>
                                </div>
                                {!! Form::close() !!}
                                <hr>
                                <label>حذف حساب کاربری</label>
                                <p>در کادر زیر بنویسید delete تا دکمه حذف حساب کاربری فعال شود.</p>
                                <p class="dim">با حذف حساب خود  <b> همه </b>پروژه ها و وظایف (دستورات) حذف خواهند شد. </p>
                                <input v-model="delete_text" type="text" class="form-control">
                                <br>
                                <div v-if="delete_text == 'delete' ">
                                    <button v-on:click="delete()" id="delete-account" class="btn btn-danger">حذف کردن حساب کاربری من</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </section>
            </div>
        </div>
    </div>

    <script>
        megaMenuInit();
    </script>
    <script src="{{ asset('assets/js/controllers/user.js') }}"></script>
@stop()
