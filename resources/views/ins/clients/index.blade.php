@extends('templates/ins/master')

@section('content')
    
<div id="client">
    <div class="row">
        <div class="col-xs-12 page-title-section">
            <h1 class="pul-right">کلاینت ها</h1>
            <a v-on:click="showCreateForm()" class="btn btn-primary pull-left" title="ساخت کلاینت جدید"> +‌ کلاینت جدید</a>
            <div class="clearfix"></div>
        </div>
    </div>

    <template v-if="clients.length != 0">
        <div class="row">
            <div class="col-xs-12">
                <div class="mega-menu">
                    <div class="links">
                        <a v-for="client in clients" data-id="client_@{{client.id}}" href="">
                            @{{client.name}}
                        </a>
                    </div>
                    <div class="content">
                        <div v-for="client in clients" class="item" id="client_@{{client.id}}" title="ویرایش کلاینت">
                            <header>
                                <div class="client client-info-@{{$index}} page-title-section">
                                    <h2 class="pull-right">@{{client.name}} <a v-on:click="startClientEditMode($index)" class="show-on-hover btn btn-default" title="ویرایش کلاینت"><i class="ion-edit"></i></a></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div>
                                    <p><label>نام مخاطب:  </label> @{{client.point_of_contact}}</p>
                                    <p><label>شماره تماس مخاطب:  </label> @{{client.phone_number}}</p>
                                    <p><label>ایمیل مخاطب: </label> <a href="mailto:@{{client.email}}">@{{client.email}}</a></p>
                                </div>
                            </header>
                            <hr>
                            <span v-on:click="showNewProjectForm(client.id, $index)" title="پروژه جدید" class="btn btn-default pull-left">پروژه جدید</span>
                            <template v-if="client.projects.length > 0">
                                <h4>پروژه ها</h4>
                                <table class="table">
                                    <thead>
                                    <tr>‍
                                        <td>#</td>
                                        <td>نام</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="project in client.projects">
                                        <td>@{{ $index + 1 }}</td>
                                        <td><a href="{{ route('projects.show', ['id' => '']) }}/@{{ project.id }}">@{{ project.name }}</a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </template>
                            <br>
                            <div class="clearfix"></div>
                            <hr><br><br>
                            <span v-on:click="deleteClient(client, $index)" class="btn btn-danger pull-right">حذف @{{ client.name }}</span>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </template>

    <template v-if="clients.length == 0">
        <div class="clearfix"></div>
        <p class="alert alert-warning">
            وقتی کلاینتی ایجاد نمایید در اینجا لیست کلاینت ها را مشاهده خواهید کرد. <a v-on:click="showCreateForm()">حالا</a>.
        </p>
    </template>

	{{-- FORMS --}}
	<div class="popup-form new-client">
		<header>
			<p class="pull-right">کلاینت جدید</p>
			<div class="actions pull-right">
				<i title="Minimze "class="ion-minus-round"></i>
				<i title="Close" class="ion-close-round"></i>
			</div>
			<div class="clearfix"></div>
		</header>
		<section>
			<form>
				<span v-if="msg.success != null" class="status-msg success-msg">@{{ msg.success }}</span>
				<span v-if="msg.error != null" class="status-msg error-msg">@{{ msg.error }}</span>
				<input v-model="client.name" placeholder="نام کلاینت" type="text" class="form-control first">
				<input v-model="client.email" placeholder="ایمیل" type="text" class="form-control">
				<input v-model="client.point_of_contact" placeholder="نکته یا یادداشت" type="text" class="form-control">
				<input v-model="client.phone_number" placeholder="شماره تماس" type="text"class="form-control">
			</form>
		</section>
		<footer>
			<a v-on:click="create(client,true)" class="btn btn-primary pull-right">ذخیره</a>
			<div class="clearfix"></div>
		</footer>
	</div>
	<div class="popup-form new-project">
		<header>
			<p class="pull-left">پروژه جدید</p>
			<div class="actions pull-right">
				<i title="Minimze "class="ion-minus-round"></i>
				<i title="Close" class="ion-close-round"></i>
			</div>
			<div class="clearfix"></div>
		</header>
		<section>
			<form>
                <span v-if="msg.success != null" class="status-msg success-msg">@{{ msg.success }}</span>
                <span v-if="msg.error != null" class="status-msg error-msg">@{{ msg.error }}</span>
				<input v-model="newProject.name" placeholder="نام" type="text" class="form-control first">
			</form>
		</section>
		<footer>
			<a v-on:click="createProject(true)" class="btn btn-primary pull-right">ذخیره</a>
			<div class="clearfix"></div>
		</footer>
	</div>
	<div style="z-index: 20" class="popup-form update-client">
        <header>
            <p class="pull-left">به روز رسانی کلاینت</p>
            <div class="actions pull-right">
                <i title="Minimze "class="ion-minus-round"></i>
                <i title="Close" class="ion-close-round"></i>
            </div>
            <div class="clearfix"></div>
        </header>
        <section>
            <form>
                <span v-if="msg.success != null" class="status-msg success-msg">@{{ msg.success }}</span>
                <span v-if="msg.error != null" class="status-msg error-msg">@{{ msg.error }}</span>
                <span class="status-msg"></span>
                <input v-model="currentClient.name" placeholder="نام کلاینت" type="text" class="form-control first">
                <input v-model="currentClient.email" placeholder="ایمیل" type="text" class="form-control">
                <input v-model="currentClient.point_of_contact" placeholder="یادداشت (نکته)" type="text" class="form-control">
                <input v-model="currentClient.phone_number" placeholder="شماره تماس" type="text"class="form-control">
            </form>
        </section>
        <footer>
            <a v-on:click="updateClient()" class="btn btn-primary pull-right">به روز رسانی</a>
            <div class="clearfix"></div>
        </footer>
	</div>
</div>


	<script src="{{ asset('assets/js/controllers/client.js') }}"></script>
@stop()