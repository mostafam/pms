@extends('templates.outs.auth')

@section('content')

    <div class="special-form">
        {{--<a href="{{ route('home') }}"><img src="{{ \App\Helpers\Helpers::logoUrl()  }}" alt=""></a>--}}
        <h3 class="text-center">عضویت</h3>
        @if ($errors->first())
            <span class="status-msg error-msg">{{ $errors->first() }}</span>
        @endif
        <hr>
        {!! Form::open(array('action' => 'UsersController@register')) !!}
        <div class="form-group">
            <label for="fullName" class="color-primary">نام و نام خانوادگی: </label>
            {!! Form::text('fullName', null, array('class' => 'form-control', "placeholder" => "نام و نام خانوادگی", "autofocus" => "true" )) !!}
        </div>
        <div class="form-group">
            <label for="email" class="color-primary">ایمیل: </label>
            {!! Form::text('email', null, array('class' => 'form-control', "placeholder" => "ایمیل" )) !!}
        </div>
        <div class="form-group">
            <label for="password" class="color-primary">رمز:‌ </label>
            {!! Form::password('password', array('class' => 'form-control', "placeholder" => "رمز" )) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('عضویت', array('class' => 'btn btn-primary btn-wide' )) !!}
        </div>
        {!! Form::close() !!}
        <p>حساب کاربری داری؟ <a href="{{ route('login') }}"> ورود </a></p>
    </div>

@stop


