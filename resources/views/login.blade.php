@extends('templates.outs.auth')

@section('content')

  <div class="special-form">
      {{--<a href="{{ route('home') }}"><img src="{{ asset('assets/img/logo.jpg') }}" alt=""></a>--}}
      <h3 class="text-center">ورود</h3>
      @if ($errors->first())
          <span class="status-msg error-msg">{{ $errors->first() }}</span>
      @endif
      <hr>
    {!! Form::open(array('action' => 'UsersController@login')) !!}
        <div class="form-group">
            <label for="email" class="color-primary">ایمیل:</label>
            {!! Form::text( 'email', null, array('class' => 'form-control', "placeholder" => "ایمیل","autofocus" => "true" )) !!}
        </div>
        <div class="form-group">
            <label for="password" class="color-primary">رمز</label>
            {!! Form::password( 'password', array('class' => 'form-control', "placeholder" => "رمز" )) !!}
        </div>
        <div class="form-group">
            {!! Form::submit( 'ورود', array('class' => 'btn btn-primary btn-wide')) !!}
        </div>
    {!! Form::close() !!}
    <p>حساب کاربری ندارید؟  <a href="{{ route('register') }}">عضویت</a></p>
  </div>

@stop