@extends('templates/ins/master')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <h1>ادمین</h1>
            <p>تعداد کاربران : <span class="badge">{{ $n_users  }}</span></p>
            <p>تعداد کلاینت ها : <span class="badge">{{ $n_clients  }}</span></p>
            <p>تعداد پروژه ها : <span class="badge">{{ $n_projects  }}</span></p>
            <p>تعداد وظایف : <span class="badge">{{ $n_tasks  }}</span></p>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>نام کامل</th>
                    <th>ایمیل</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->full_name }}</td>
                        <td>{{ $user->email }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop()